# **SNORT**
![logo_porquet](../images/snort_logo.jpg)  
  

NIDS és també conegut com a Sistema de detecció d'intrusos en una Xarxa. Busca detectar anomalies que indiquin un risc potencial. 

# Motor de snort
![snort_engine](../images/engine.png)  
  

- **Decodificador de paquets**: És la part encarregada d'agafar tots els paquets d'una xarxa, desde les diferents interfícies.

- **Preprocessadors**: És la part encarregada de preparar les dades, ajuntant paquets, per tal que les regles del motor de detecció puguin fer *match* de forma correcta.

- **Motor de detecció**: És la part més important, ja que s'encarrega de contrastar els paquets que venen dels preprocessadors amb les seves regles.

- **Logging**: Depenent del que es detecti amb les regles, els logins s'encarreguen de loguejar el paquet o generar una alerta.

- **Plugins de sortida**: S'agafa la sortida generada per l'alerta i l'emmagatzema en diferents formats, reaccionant a ella, fent que s'hagi d'enviar un mail, o inserint una nova entrada a una base de dades.

# Modes de snort

- **Sniffer mode**
- **Packet Logger mode**
- **Network Intrusion Detection System mode(NIDS)**

# Sniffer mode
```
root@linux:~# snort -v -A console -c /etc/snort/snort.conf -i eno1
```


- ```-v ```: Mode *sniffer*.

- ```-A console```: Aquí fem referència a que l' *output* de l'ordre es farà a traves de la terminal.

- ```-c /etc/snort/snort.conf```: Aquí fem referència a la configuració que Snort ha d'utilitzar per funcionar.

- ```-i eno1```: Interficie per la que Snort escoltarà i analitzarà el tràfic.

```
05/15-13:47:13.411215 192.168.1.33:35704 -> 82.196.7.188:443
TCP TTL:64 TOS:0x0 ID:57009 IpLen:20 DgmLen:206 DF
***AP*** Seq: 0xBD91E2EF  Ack: 0x934759BB  Win: 0x1F5  TcpLen: 32
TCP Options (3) => NOP NOP TS: 3711423327 293179851 
=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+

05/15-13:47:13.450120 82.196.7.188:443 -> 192.168.1.33:35704
TCP TTL:51 TOS:0x0 ID:42760 IpLen:20 DgmLen:52 DF
***A**** Seq: 0x934759BB  Ack: 0xBD91DD60  Win: 0xEB  TcpLen: 32
TCP Options (3) => NOP NOP TS: 293179861 3711423327 
=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+

```

# Packet Logger mode
Encenem Snort:  

```
root@linux:~# snort -q -l ./snort_logger/ -i eno1 
```
- ```-q```: Amb aquesta part únicament estem avisant a Snort que no ens mostri res per pantalla.

- ```-l ./dir_2_log/```: Amb questa part indiquem el directori de *log*.

- ```-i eno1```: Interficie per la que Snort escoltarà i analitzarà el tràfic.
  
```
root@linux:~# ls snort_logger/

snort.log.1621130984  snort.log.1621131346  snort.log.1621131362

```
Llegim un paquet:  

```
root@linux:~# snort -r snort_logger/snort.log.1621130984
```
- ```-r```: Li diem a snort que llegeixi un fitxer de logs.

```
root@linux:~# wireshark snort_logger/snort.log.1621130984
```
![captura_wireshark](../images/wireshark.png)


# Network Intrusion Detection System mode(NIDS)

```
alert tcp any any -> any any (msg:"!!! TCP detectat !!!";sid: 1000001;)
```
- **Header**: 
	- ```alert```: Ens alertarà.

    - ```log```: Emmagatzemarà els paquets.

    - ```pass```: Ignora el paquet.

    - ```drop```: Bloqueja i emmagatzema el paquet. 

    - ```reject```: Bloqueja el paquet, el registra i envia una resposta.

    - ```sdrop```: Bloqueja el paquet però no l'emmagatzema.

- **Protocol**  
  
- **IP** (Origen)
- **Port** (Origen) 
- ```->```
- **IP** (Destí)
- **Port** (Destí)
- **Opcions de Snort**

```
root@linux:~# snort -A console -c /etc/snort/snort.conf -i eno1
```  
- ```-A console```: Aquí fem referència a que l' output de l'ordre es farà a traves de la terminal.
- ```-c /etc/snort/snort.conf```: Aquí fem referència a la configuració que Snort ha d'utilitzar per funcionar.  
- ```-i eno1```: Interficie per la que Snort escoltarà i analitzarà el tràfic.  

```
05/17-04:37:01.494250  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494369  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494487  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494606  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494725  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494844  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494963  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.495082  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.495201  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.495320  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.495438  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262

```
