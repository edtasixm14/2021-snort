# **SNORT**
![logo_porquet](./images/snort_logo.jpg)

## Que és?
Snort és un *Sistema de Detecció/Prevenció d'Intrusos en Xarxa* (IDS/IPS), és un programa de codi obert, *open source*, i gratuït, tot i que te una version amb regles de pagament. Va ser creat el 1998 per Martin Roesch, cofundador de *Sourcefire*. Actualment està sent desenvolupat per *Cisco Systems* quí va comprar *Sourcefire* el 2013.  
  


## Per què **SNORT**?
Snort és una eina interessant degut a la seva versatilitat i el que ens permet fer. És capaç d'analitzar en temps real el tràfic d'una xarxa, permetent establir regles que faran *match* amb el tràfic. Aquestes regles ens permeten tenir la nostra xarxa ben protegida ja que podem detectar múltiples atacs, o intents, i prevenir-nos dels mateixos.  
  


## Els 3 modes:
Com ja s'ha explicat abans **Snort** és molt versatil, aquesta qualitat es deu a que pot configurar-se en 3 modes diferents, permetent adaptar-se a les necessitats específiques de l'entorn en el que treballem. Aquests modes són:

- **Sniffer mode**: En aquest mode Snort monitorejarà tot el tràfic que està circulant per la xarxa que indiquem mentre ens mostra per tantalla els paquets.

- **Packet Logger mode**: En aquest mode emmagatzemarem el tràfic de xarxa per tal que despres s'en pugui fer un analisi més detallat.

- **Network Intrusion Detection System mode**: En aquest mode Snort utilitza un conjunt de regles de les que prendrà mesures per a tractar el tràfic que faci *match* amb aquestes.  
  


## Vull instalar **SNORT**:
En el cas de tenir un sistema **Ubuntu 20.04 TLS** l'instalació és ben simple:  
  
```
root@linux:~# apt-get install -y snort
```  
  
Acte seguit Snort començarà a instal·lar-se. Durant aquest temps ens preguntarà per la interfície per defecte per la que ha d'escoltar, ens recomanarà una però si cal es pot canviar. Aquests parametres més endavant es podran modificar amb les ordres d'execució que utilitzarem:  

![ask_interfaces](./images/ask_interface.png)  

Un cop haguem configurat la interficie per defecte ens demanarà configurar la xarxa per defecte per la que analitzarà el tràfic:  

![ask_net](./images/ask_net.png)  

Igual que a la pregunta anterior, aquest parametre es pot reconfigurar més endavant.  
Acabem d'instal·lar Snort al nostre sistema.



## Configuracions basiques de **/etc/snort/snort.conf**:
Per a que Snort funcioni correctament cal comentar algunes línies del fitxer de configuració, així quan l'executem evitarem que ens mostrin errors. Les línies són les següents:
  
- La línia que conte les regles referents a la càrrega de regles dinàmiques:  

	```
	#dynamicdetection directory /usr/lib/snort_dynamicrules
	```
- Les línies referents al preprocessador de reputació:  

	```
	# Reputation preprocessor. For more information see README.reputation
	#preprocessor reputation: \
	#   memcap 500, \
	#   priority whitelist, \
	#   nested_ip inner, \	
	#   whitelist $WHITE_LIST_PATH/white_list.rules, \
	#   blacklist $BLACK_LIST_PATH/black_list.rules
	```
- Les línies de la secció 7 del fitxer referents als *includes* de les regles locals:  

	```
	###################################################
	# Step #7: Customize your rule set
	# For more information, see Snort Manual, Writing Snort Rules
	#
	# NOTE: All categories are enabled in this conf file
	###################################################
	```

- Amés de les configuracions basiques també hi ha altres parametres que ens poden resultar molt útils, ja que podem definir petites variables que ens ajudaran a emmagatzemar dades que despres podem utilitzar a la construcció de regles. Algunes de les variables són:  
  
	- ```ipvar [nom_variable] [opcions_d_variables]```: D'aquesta forma el que podem fer és crear una variable de nom ```nom_variable```, a la que podem atrivuir diferents valors com una IP, una altre variable o negar variables ja existents(**!**). Per exemple:  
		  
		- ```ipvar HOME_NET any```  
		- ```ipvar SSH_SERVER $HOME_NET```  
		- ```ipvar EXTERNAL_NET !$HOME_NET```  

	- ```portvar [variable_ports] [nº]```: D'aquesta forma el que podem fer és crear una variable de nom ```variable_ports```, a la que podem atrivuir el valors tants ports com volguem. Per exemple:
		- ```portvar HTTP_PORTS [80,81,311,383,591,593]```
		- ```portvar SHELLCODE_PORTS !80```

- Comprovem que les modificacions del fitxer de configuració s'han fet correctament:  

	```
	root@linux:~# snort -T -c /etc/snort/snort.conf 
	```



## Abans de començar:

Per entendre com Snort pot fer tot el que pot fer, cal entendre com ho pot fer. Diem que el motor de Snort es composa de diferents capes, igual que una ceba, i cadascuna d'aquestes capes s'encarrega d'una determinada tasca. Les capes són les següents:
  
![ask_interfaces](./images/engine.png)  

- **Decodificador de paquets**: És la part encarregada d'agafar tots els paquets d'una xarxa, desde les diferents interfícies.

- **Preprocessadors**: És la part encarregada de preparar les dades, ajuntant paquets, per tal que les regles del motor de detecció puguin fer *match* de forma correcta. En alguns casos aquests preprocessadors poden fins i tot encarregar-se de buscar anomalies en les capçaleres dels paquets que indiquin que estan sent enviats per un atacant, d'aquesta forma Snort funciona molt més eficientment i el paquet no ha de ser analitzat pel motor de detecció per ser descartat.

- **Motor de detecció**: És la part més important, ja que s'encarrega de contrastar els paquets que venen dels preprocessadors amb les seves regles. Aquestes són definides pensant en un tràfic determinat, és a dir que si el tràfic no fa *match* el paquet s'ignora, independentment de la seva intencionalitat.

- **Logging**: Depenent del que es detecti amb les regles, els logins s'encarreguen de loguejar el paquet o generar una alerta. Els paquets que es loguegin s'emmagatzemaran en un fitxer de text que tingui format *tcpdump*.

- **Plugins de sortida**: Un cop el paquet ja s'ha analitzat i ha fet *match*, s'hi n'ha fet, s'agafa la sortida generada per l'alerta i l'emmagatzema en diferents formats, reaccionant a ella, fent que s'hagi d'enviar un mail, o inserint una nova entrada a una base de dades. Els *plugins* de sortida més comuns són:
	- Bases de Dades (MySQL, Postgres, etc)
	- Syslog
	- XML  
  
Tot i que les configuracions necessàries per no mostrar errors ja s'han completat, per estalviar temps en un futur podem modificar els apartats seguents:
  
```
ipvar HOME_NET [IP/CIDR de la xarxa local]
ipvar EXTERNAL_NET !$HOME_NET
```

## Sniffer mode:
Quan Snort treballa en mode *sniffer* es dedica a analitzar tots els paquets d'una xarxa, mostrant per pantalla les capçaleres dels paquets **IP** / **TCP** / **UDP** / **ICMP**. L' *output*, a diferència de Wireshark, no es mostra de forma gràfica sino que es fa través de la terminal, això si, en temps real.
  
Per engegar Snort en mode *sniffer* escribim la comanda següent:  

```
root@linux:~# snort -v -A console -c /etc/snort/snort.conf -i eno1
```  
Com podem veure la comanda es composa de diferents elements:  

- ```-v ```: Aquesta part és la més important ja que indica a snort que s'activarà en mode *sniffer*.

- ```-A console```: Aquí fem referència a que l' *output* de l'ordre es farà a traves de la terminal, per tant es podrà veure en temps real el que Snort està analitzant.

- ```-c /etc/snort/snort.conf```: Aquí fem referència a la configuració que Snort ha d'utilitzar per funcionar, en el nostre cas la configuració es pren del fitxer per  defecte.

- ```-i eno1```: Finalment arribem a l'apartat de l'ordre que indica la interficie per la que Snort escoltarà i analitzarà el tràfic.

Exemple:

```
05/15-13:47:13.411215 192.168.1.33:35704 -> 82.196.7.188:443
TCP TTL:64 TOS:0x0 ID:57009 IpLen:20 DgmLen:206 DF
***AP*** Seq: 0xBD91E2EF  Ack: 0x934759BB  Win: 0x1F5  TcpLen: 32
TCP Options (3) => NOP NOP TS: 3711423327 293179851 
=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+

05/15-13:47:13.450120 82.196.7.188:443 -> 192.168.1.33:35704
TCP TTL:51 TOS:0x0 ID:42760 IpLen:20 DgmLen:52 DF
***A**** Seq: 0x934759BB  Ack: 0xBD91DD60  Win: 0xEB  TcpLen: 32
TCP Options (3) => NOP NOP TS: 293179861 3711423327 
=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
```
En aquest exemple engeguem Snort en mode *sniffer* i el deixem una estona capturant paquets, com els del requadre anterior.  
Com es pot veure Snort fa una anàlisi bastant exhaustiva dels paquets, del que podem extraure informació com el **TTL**, la **IP** i **PORT** de procedència/destí, la longitud dels paquets, etc ...



## Packet Logger mode:
El mode *Packet Logger* és ben simple ja que de fet funciona com el mode *sniffer* però ens permet registrar i emmagatzemar el que haguem analitzat.  
  
Per evitar futurs problemes és recomanable tenir un directori destinat únicament als fitxers que es generin pel mode *packet logger*, per tant executem l'ordre següent al directori de treball desitjat:  

```
root@linux:~# mkdir snort_logger
```
Un cop hem creat el directori destinat a emmagatzemar els paquets, només cal engegar Snort en mode *packet logger*:  

```
root@linux:~# snort -q -l ./snort_logger/ -i eno1 
```
Per poder entendre l'ordre anterior cal saber que amb el mode *packet logger* el que fem és redirigir la sortida estàndard del mode *sniffer* a un fitxer amb format *pcap*.  
Un com hem entès com funciona, entendrem de que es composa l'ordre:  

- ```-q```: Amb aquesta part únicament estem avisant a Snort que no ens mostri res per pantalla, ni tan sols els missatges d'error.

- ```-l ./dir_2_log/```: Amb questa part indiquem el directori de *log*, el qual omplirem amb un fitxer que contindrà l'anàlisi realitzat.

- ```-i eno1```: Igual que al mode anterior aquí únicament indiquem l'interfície per la que ha d'escoltar.  

Un cop hem generat el fitxer, si entrem al directori de *logs* podem veure que els fitxers tenen un nom extrany, però únicament es tracta d'una seqüència de números referents a l' **unix time stamp** del moment en el que s'ha generat el fitxer:  

```
root@linux:~# ls snort_logger/

snort.log.1621130984  snort.log.1621131346  snort.log.1621131362
```

Ara el que toca és poder llegir l'anàlisi realitzat, per tant per poder llegir el fitxer en format *pcap* s'ha d'executar l'ordre següent:  

```
root@linux:~# snort -r snort_logger/snort.log.1621130984
```

Entendre aquesta ordre és ben simple, ja que amb ```-r``` únicament estem indicant a snort que llegeixi el fitxer que li indiquem a continuació.  
  
Aquest és un dels punts en que Snort ens sorpren amb la seva versatilitat. Quan generem un fitxer en format *pcap* permetem que en cas de no ser gaire fans de la terminal poguem llegir el format des de l'eina **Wireshark**, ja que  Wireshark ens permet analitzar el tràfic de fitxers *pcap*. Per tant per poder llegir el tràfic des d'una aplicació gràfica podem fer el següent:  
  
```
root@linux:~# wireshark snort_logger/snort.log.1621130984
```
Acte seguit Wireshark s'engegarà interpretant el nostre fitxer *pcap*:  

![captura_wireshark](./images/wireshark.png)

## Network Intrusion Detection System mode:
En aquets mode, el que fa Snort és analitzar el tràfic de xarxa contrastantlo amb regles. L'explicació pot ser simple, però el seu funcionament és el més complex dels tres modes, degut a que si volem que el motor d' *Snort* detecti el tràfic desitjat les regles han d'haberse escrit perfectament.    

Per poder entendre l'importancia de redactar les regles correctament cal saber que un paquet, tot i tenir intencions *dolentes*, pot travessar el motor d' *Snort* completament i no ser detectat si les regles no estan orientades a detectar aquest tràfic en concret.  
  
Per això un cop hem entes l'importancia del bon redactat d'aquestes regles, hem daprendre a escriureles. Les regles d' *Snort* estan formades pels seguents components:

```
alert tcp any any -> any any (msg:"!!! TCP detectat !!!";sid: 1000001;)
```
El contingut, d'esquerra a dreta, és el següent:  

- **Header**: És el primer que veiem i defineix que es farà amb el tràfic que fagi *match*. Hi ha diferents *headers*:
	- ```alert```: Genera una alerta que mostrarà  per la sortida estàndard.  

    - ```log```: Emmagatzema el paquet en el directori de *log* per defecte.  

    - ```pass```: Ignora el paquet.  

    - ```drop```: Bloqueja i emmagatzema el paquet. 

    - ```reject```: Bloqueja el paquet, el registra i envia una resposta.   

    - ```sdrop```: Bloqueja el paquet però no l'emmagatzema.    

```
05/17-04:37:01.494250  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494369  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494487  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
```  
 Aquesta és la sortida esta que generaria qualsevol de les capçaleres quan fes match amb alguna de les ordres que hem indicat, sempre que la capçalera tingui sortida per *stdout*.

```
root@linux:~# ls snort_logger/

snort.log.1621130984
```
Això seria un *ls* del directori de logs on la propia regla ha emmagatzemat els paquets que han fet *match*. Cada fitxer conté el conjunt del trafic de xarxa analitzat.



- **Protocol**: En aquest cas el que indiquem és el protocol que fa servir el tràfic que volem detectar. Els protocols que Snort detecta són el **TCP**, l' **UDP**, l' **ICMP** i l' **IP**.

- **IP**(Origen)  
    
- **Port**(Origen)  
- ```->```: Operador direccional. També existeix el operador direccional ```<>``` el qual indica que cal tenir compte amb l'ordre en ambdos sentits, la qual cosa es interesant si el que volem fer és *loguejar* paquets quan ens interesen els dos sentits d'una combersa com a una sessió telnet.
- **IP**(Destí)  
    
- **Port**(Destí)  
    

**Opcions de Snort** :   
 
- **msg**: Indiquem el missatge que ens mostrarà per pantalla quan el tràfic fagi *match*.
- **sid**: És l'identificador únic de cada regla, **NO ES PODEN REPETIR**.

Algunes de les opciones de snort que podem incloure, amés de les que són a l'ordre són les seguents:    

- ```flags:S```: Aquí observarem els paquets amb un flag de SYN.  

- ```track by_src ```: Aquí diem que farem un seguiment dels paquets que arribin des d'un mateix origen.  

- ```count 420 ```: Aquí el que fem es comptar la quantitat de paquets que ens arribem, es interesant saber aixó si el que es vol és saber si estem tenint un tràfic irregular.
  
D'aquestes mateixes opcions n'hi ha una gran quantitat, ja que el que interesa és que aquestes regles siguin lo més eficients posibles. L'eficiencia a l'hora d'escriure les ordres és important, ja que ens interesa gastar els menys recursos posibles.  

Així doncs, un cop hem escrit la regla cal guardarla en un fitxer com ```file_name.rules``` dins del directori de regles (```/etc/snort/rules```), i afegir al final del fitxer de configuracio ```include ruta/file_name.rules ```. Aquest tipus de fitxers solen tenir una gran quantitat de regles i solen estar orientats a detectar determinats tipus de tràfic.  
  
Un cop hem escrit el nostre fitxer de regles el que cal es executar Snort:
  
```
root@linux:~# snort -q -A console -c /etc/snort/snort.conf -i eno1
```
En aquest mode Snort interpreta que l'opció ```-q```, que engegara snort en mode NIDS, combinada amb ```-A console``` demana que unicament es mostri per consola els missatges que generin les regles.  
  
L' *output* de la regla escrita anteriorment seria el següent:  

```
05/17-04:37:01.494250  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494369  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494487  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494606  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494725  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494844  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.494963  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.495082  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.495201  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.495320  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262
05/17-04:37:01.495438  [**] [1:1000000:0] !!! TCP detectat !!! [**] [Priority: 0] {TCP} 74.125.97.169:443 -> 192.168.1.33:46262

```

Aquesta forma d'escriure regles és realment simple, i com ja he esmentat avans, molt tràfic malintencionat podria no ser interceptat. Quan s'aprofundeix en el funcionament de Snort podem descobrir gran quantitat d'opcions que ens permeten ser més eficients a l'hora de detectar el tràfic.  
  
Com ja he esmentat anteriorment, el fet de que Snort sigui de codi lliure i gratuït fa que tingui una gran comunitat de "fans" que estan disposats a compartir el seu coneixement, i les seves regles. És per això que fins i tot a la pàgina oficial d'Snort podem veure com hi ha un apartat destinat a descarregar regles, les quals han sigut escrites de tal forma que siguin el més eficient possibles.  
  
Aquesta pàgina en concret ens proveeix d'una gran quantitat de regles agrupades pel tipus de tràfic pel que estàn orientades a detectar:
  
https://rules.emergingthreats.net/open/snort-2.9.0/rules/ 

## Conclusió:
Com he pogut esbrinar Snort és una eina interesantíssima. El principal atractiu d'aquesta resideix en el seu mode NIDS, tot i que els altres són molt utils també, però funcionen més com a eines complementaries i no tant com principals. Això s'ha fet notar especialment a mida que he aprofundit en l'elaboració de regles, la inmensitat d'opcions que ofereix snort és molt considerable i et fa entendre per que es continua implementant encara a dia d'avuí, més de 20 anys despres de que és creés.  
En general ha estat un projecte que m'ha ajudat a entendre, i aprendre, com funcionen els NIDS. També a reforçar els coneixements "basics" de xarxes informàtiques que tenia, ja que moltes de les ordres per poder entendrelas cal tenir un coneixement mínim, tot i així la comunitat que s'ha format sobre aquest software és realment soprenent ja que ajuda sense cap tipus de vergonya als *no iniciats*, sent molt més facil elaborar ordres i aprendre noves opcions.


## Bibliografia/Webgrafia :
https://openwebinars.net/academia/aprende/seguridad-redes-snort/  

https://usuaris.tinet.cat/sag/lsnort.htm
  

http://books.msspace.net/mirrorbooks/snortids/0596006616/snortids-CHP-3-SECT-4.html
  

https://www.sbarjatiya.com/notes_wiki/index.php/Snort_modes
  

http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node4.html
  

http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node1.html
  
https://www.linuxparty.es/57-seguridad/6000-el-sistema-de-deteccion-de-intrusos-snort--windows-y-linux-.html#:~:text=Snort%20puede%20funcionar%20en%3A,Snort%20para%20un%20posterior%20an%C3%A1lisis.
