# DEMO:

## INDEX:
1. [Port Scan](#port_scan)
2. [DDos Attack] (#ddos)
 
## Port Scan:
El *PORT SCAN* és un atac que sonsisteix en el escaneig automatic de ports, l'objectiu és trobar ports oberts en la victima per tal de tenir un vector d'atac.  
  
En aquesta demostració el que faré serà escriure una regla de Snort i descriure el seu funcionament i funcionalitat, per tal d'entendre com Snort es capaç de detectar l'atac i prevenirlo.  
  
La regla que farem servir serà la seguent:  
  
```
alert tcp any any -> 192.168.122.1 any (ack: 0; msg: “NMAP SCAN”; rsid:1000005; rev:1;)
```  
  
Com es pot veure just a dalt, la regla es composa de les següents parts :  
  
- `alert`: Aquesta es la capçalera que ens indica que farà SNORT amb els paquets amb els que fagi *match*.  
- `tcp` : Protocol sobre el que SNORT haurà de vigilar.  
- `any` : Xarxa exterior de la que arribaran els paquets, en aquest cas ens dona igual la que sigui.  
- `any` : Port exterior del que arribaran els paquets, en aquets cas ens dona igual del que provingui.  
- `->`  
- `$HOME_NET` : Variable definida al fitxer de configuració que correspon a la xarxa local, en aquets cas la nostra xarxa local serà la farà de victima del atac.  
- `any` : Port destinatari del paquet, en aquest cas ens dona igual quin sigui el port.  
  
Un cop hem definit les variables simples de les regles de SNORT, explicarem les opcions que hem introduït per tal de que l'escaneig de port sigui detectable:  
  
- `ack:0` : El que indiquem que volem que SNORT comprobi es que la *flag* del protocol TCP sigui ACK, i el valor d'aquesta flag sigui igual a 0, que es el valor que dona **nmap** als *TCP ping scan*.
- `msg:" ... "` : És el missatge que mostrarem quan un paquet fagi *match*.
- `rsid:_ID_` : Id de la regla.
- `rev:1` : Versió de la regla.


#### Atacant:
L'atacant ha de poder excutar aquesta ordre per tal de generar un escaneig de port:
  
```
snort_host1@linux:~$ sudo nmap -sN 192.168.122.1
```

## DDOS(Distribute Denial Of Service):
La seguent demos consisteix a simular un atac de denegació de serveis. Consisteix en un conjunt de *hosts* clients enviant paquets a un *hots-servidor* determinat per tal de que aquest *host-servidor* es saturi i no pugui proporcionar el servei de forma eficient.  
  
L'ordre de Snort següent, depenent del *header*, ens pot proporcionar protecció davant dels pings que ens fagin els atacants:
  
```
alert tcp any any -> 192.168.122.1 any (msg:"Posible atac DDOS"; flags:S; detection_filter: track by_dst, count 200, seconds 10; sid:1000007;)
```  
  
Entenem que l'explicaciò basica de l'ordre ja s'enten, ja que es la mateixa que a l'ordre següent, per tant explicarem directament les opcions de SNORT:  
  
- `msg:"..."`: Aquí s'escriu el missatge que l'ordre ens mostrarà.  
- `flags:S`: En aquesta part el que fem és buscar les flags del protocol TCP que corresponen a SYN (sincronització). A questa flag es la que primer s'envia quan hi ha una conversació mitjançant TCP, per tant es llogic que un atacant utilitzant un conjunt de hosts en una *botnet*, el que intenti fer sigui saturar un servei enviant la major quantitat possible de paquets amb SYN al port del servei.  
- `detection_filter: track by_dst, count 200, seconds 1`: Aquí el que indiquem és que el filtre de detecció cribi pels seguents filtres:  
  - `track by_dst`: Comprobem que la trama i tots els paquets dels que portarem un recompte, tinguin un mateix objectiu, d'aquí el fet de *by_dst* (destination).
  - `count  200`: Comprobem que arribin 200 paquets. Aquesta opció es complementa amb la seguent, la qual delimita per temps aquesta mateixa opció.
  - `seconds 1`: Aquesta opció delimita a 1 segon la quantitat de paquets que volem vigilar que arribin.      
- `isd:_ID_`: Id de la regla.

  
Per tant en resum, el que tenim és una opció que el que vigila es que no hi hagi més de 200 paquets SYN en un marge de 1 segon, a una mateixa IP destí.

#### Atacant:

L'atacant el que ha de fer és executar la seguent ordre, que pot requerir de l'instalació de l'eina `hping3` que genera paquets TCP/IP aleatoris:  
  
```
snort_host1@linux:~$ sudo hping3 -S --flood -V -p 80 192.168.122.1
```

